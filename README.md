# Court Calendar

Unofficial court calendars in subscribable and .ics format.

## Calendars

### BC Supreme Court

Source: https://www.bccourts.ca/supreme_court/scheduling/

View: https://calendar.google.com/calendar/embed?src=t3e5j6di1okdeotbo91us0oqeg%40group.calendar.google.com&ctz=America%2FVancouver

Subscribe: https://calendar.google.com/calendar/ical/t3e5j6di1okdeotbo91us0oqeg%40group.calendar.google.com/public/basic.ics

### BC Provincial Court

Source: https://provincialcourt.bc.ca/about-court/calendar

View: https://calendar.google.com/calendar/embed?src=irvs1eckr5ip938lsvv2150cv4%40group.calendar.google.com&ctz=America%2FVancouver

Subscribe: https://calendar.google.com/calendar/ical/irvs1eckr5ip938lsvv2150cv4%40group.calendar.google.com/public/basic.ics

### Download .ics

Yearly .ics files can be downloaded above.

## Updates

Dates and events occassionally change. Please open a new issue to report any problems.

## As-is

Provided as-is for convenience. No promise of accuracy or up-to-date-ed-ness.